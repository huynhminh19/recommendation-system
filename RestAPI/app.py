from flask import Flask
from api import *

def create_app():
    app = Flask(__name__)
    app.register_blueprint(main)

    return app
