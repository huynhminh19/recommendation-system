#!/usr/bin/env python
import pika
import json

def call_recommendation(project):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='recommendation')

    channel.basic_publish(exchange='',
                          routing_key='recommendation',
                          body=project)
    print("Sent " + project)
    connection.close()

def call_add_buy(project, rating):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='add_buy')
    data = {"project": project, "rating":rating}
    channel.basic_publish(exchange='',
                          routing_key='add_buy',
                          body=json.dumps(data))
    print("Sent 'add_buy'")
    connection.close()
