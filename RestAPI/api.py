from flask import Blueprint
main = Blueprint('main', __name__)
from flask_pymongo import PyMongo, DESCENDING
from collections import OrderedDict
import json, os, logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

from flask import Flask, request, redirect, url_for, render_template
from tasks import call_add_buy, call_recommendation
from mongo import client
from werkzeug.utils import secure_filename
from flask import send_from_directory

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = set(['txt', 'csv'])

@main.route("/<string:project>/<int:user_id>/recommendation/top/<int:top>", methods=["GET"])
def top_recommendation(project, user_id, top):
    logger.debug("User %s TOP recommendation requested", user_id)
    top_recommendation = client[project].predicted_user_ratings.find_one({'user': user_id})
    top_recommendation = sorted(top_recommendation["top"], key=lambda t: t['rating'], reverse=True)[:top]

    return json.dumps(top_recommendation, encoding="utf-8")

@main.route("/<string:project>/<int:item_id>/recommendation-product/top/<int:top>", methods=["GET"])
def top_recommendation_product(project, item_id, top):
    logger.debug("User %s TOP recommendation product requested", item_id)
    top_recommendation = client[project].predicted_product_ratings.find_one({'product': item_id})
    top_recommendation = sorted(top_recommendation["top"], key=lambda t: t['rating'], reverse=True)[:top]

    return json.dumps(top_recommendation, encoding="utf-8")

@main.route("/<string:project>/<int:item_id>/sims/top/<int:top>", methods=["GET"])
def top_items_sims(project, item_id, top):
    logger.debug("item %s TOP sims requested", item_id)
    top_items_sims = client[project].sims.find({"$or":[ {'_1._1': item_id}, {'_2._1': item_id}  ]}).limit(top).sort([("_3.value", DESCENDING)])

    resutl = []
    for item in top_items_sims:
        obj = dict()
        if item['_1']['_1'] == item_id:
            obj['id'] = item['_2']['_1']
        else:
            obj['id'] = item['_1']['_1']
        obj['rating'] = item["_3"]['value']
        resutl.append(obj)

    return json.dumps(resutl, encoding="utf-8")


@main.route("/<string:project>/add", methods = ["POST"])
def add_buys(project):
    rating = request.form.keys()[0].strip()
    call_add_buy(project=project, rating=rating)
    return json.dumps(rating)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@main.route('/', methods=['GET', 'POST'])
def upload_file():
    projects = client["recommendation_admin"].projects.find()
    projects = list(projects)
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files or 'project' not in request.form:
            print('No file part')
            return redirect(request.url)
        project = request.form['project']
        file = request.files['file']
        if file.filename == '' or project == '' :
            return render_template('home.html', projects=projects, errors="Name project and File upload is required")
        for project_exist in projects:
            if project == project_exist["name"]:
                print projects
                return render_template('home.html', projects=projects, errors="Name project is already exist")
        if file and allowed_file(file.filename):
            filename = secure_filename(project)
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            call_recommendation(filename)
            return redirect(url_for('main.project_info', project=project))
    return render_template('home.html', projects=projects)

@main.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(UPLOAD_FOLDER,
                               filename)

@main.route('/project/<string:project>')
def project_info(project):
    return render_template('project.html', project=project)
