# README #

Recommendation System is a machine learning framework for developers, data scientists, and end users. 
It supports event collection, deployment of algorithms, evaluation, querying predictive results via REST APIs. 
It is based on scalable open source services like Mongo (and other DBs), RabbitMQ, Spark + Mllib.

![architecture](/EdumallRec.png)

### Install & Configuration ###

* System dependencies
	+ Python
	+ JDK
	+ Flask
	+ Mongo
	+ RabbitMQ,Pika
	+ tmuxp
	+ cherrypy
	+ numpy
	+ paste
	+ Spark, Mllib
	
	
	**1.  Install Java **
	
	```
	apt-get install oracle-java8-installer
	```
	
	**2.  pip requirements file **
	
	```
	pip install -r requirements.txt
	```
	
	**3.  Install and Start RabbitMQ **
	
```
	apt-get install rabbitmq-server
	service rabbitmq-server start
```
	
	> Default RabbitMQ running on localhost on standard port (5672).

* Configuration
* Database configuration
> mongo.py
* How to run tests
* Deployment instructions

	**1.  Start recommendation engine **
	
	```
	cd Spark
	./start_server.sh
	```
	
	**2.  Start Rest API **
	
	```
	cd RestAPI
	python server.py
	```

### Documentation ###

**1.  Recommendation engine **

**1.1  Spark/engine.py **

When the engine is initialised, we need to geenrate the ALS model for the first time. 
Optionally (we won't do it here) we might be loading a previously persisted model in order to use it for recommendations. 
Moreover, we might need to load or precompute any RDDs that will be used later on to make recommendations.
We will do things of that kind in the __init__ method of our RecommendationEngine class (making use of two private methods). 
In this case, we won't save any time. We will repeat the whole process every time the engine is creates.

```
def __init__(self):
        """Init the recommendation engine given a Spark context and a dataset path
        """
        self.projects = {}
        self.sc = init_spark_context()
        self.sqlContext = SQLContext(self.sc)
        self.ss = SparkSession(self.sc)
        self.rank = 10
        self.seed = 5L
        self.iterations = 10
        self.regularization_parameter = 0.1

        for project in client["recommendation_admin"].projects.find():
            project = project["name"]
            self.projects[project] = RecommendationProject()
            df = self.ss.read.format("com.mongodb.spark.sql.DefaultSource").option("uri", "mongodb://127.0.0.1/" + project + ".ratings").load()
            data = df.rdd
            self.projects[project].ratings_RDD = data.map(lambda l: (int(l["user_id"]),int(l["item_id"]),float(l["rating"])))
            self.__train_model(project)
```


**1.2 RabbitMQ (Spark/tasks.py) **

```
from engine import RecommendationEngine
recommendation_engine = RecommendationEngine()
```

Making recommendations

```
def recommendation(ch, method, properties, body):
	print("Received %r" % body)
	recommendation_engine.recommendation(body)
```

Adding new ratings

```
recommendation_engine.add_rating(project, [rating])
```

**1.3 Running the server with Spark (Spark/start_server.sh) **

- The important bits are:
	- Use spark-submit and not pyspark directly.
	- The --master parameters must point to your Spark cluster setup (can be local).
	- You can pass additional configuration parameters such as --total-executor-cores and --executor-memory

**2.  Rest API **

**2.1 Building a web API around our engine using Flask (RestAPI/api.py) **

Flask is a web microframework for Python. 
It is very easy to start up a web API, by just importing in in our script and using some annotations to associate our service end-points with Python functions. 

Basically we use the app as follows:
We associate the @main.route annotations defined. Each annotation is defined by (see Flask docs):
A route, that is its URL and may contain parameters between <>. They are mapped to the function arguments.
A list of HTTP available methods.

- GET /[Projectname]/[user_id]/recommendation/top/10
- Input
	- user ID
	- num of output items(1 <= n <= 20)
- Output PredictedResult
	- a ranked list of recommended itemIDs
	
- GET /[Projectname]/[item_id]/recommendation-product/top/10
- Input Query
	- item ID
	- num of recomended users(1 <= n <= 20)
- Output PredictedResult:
	- a ranked list of recommended userIDs
	
- GET /[Projectname]/[item_id]/sims/top/10
- Input Query
	- item ID
	- num of recomended items(1 <= n <= 20)
- Output PredictedResult
	- a ranked list of recommended itemIDs based on one item.
	
- POST /[Projectname]/add -d "user_id,item_id,rating"
	- Add more values for system.

**2.2 Deploying a WSGI server using CherryPy (RestAPI/server.py) **

Among other things, the CherryPy framework features a reliable, HTTP/1.1-compliant, WSGI thread-pooled webserver.
It is also easy to run multiple HTTP servers (e.g. on multiple ports) at once. 
All this makes it a perfect candidate for an easy to deploy production web server for our on-line recommendation service.
The use that we will make of the CherryPy server is relatively simple. 
This is pretty standard use of CherryPy.

**MONGODB**

	BUYS: (input ALS)
	  uid: user id (int)
	  cid: course id (int)

	USERS:
	  _id: (id in jackfruit db)
	  uid: (int) to input ALS
	  email

	COURSES:
	  _id: (id in jackfruit db)
	  cid: (int) to input alias
	  name
	  alias_name
	  code

	PREDICTED_PRODUCT_RATINGS: Output ALS.
	  product: cid courses
	  top: a ranked list of recommended itemIDs
		[{user, product, rating}, {user, product, rating}]

	PREDICTED_USER_BUYS: Output ALS.
	  user: uid users
	  top: a ranked list of recommended userIDs
		[{user, product, rating}, {user, product, rating}]

	SIMS: recommended Similar course

**INIT DATA BY CSV**

- Export data to Spark/data:

>mongoexport --db jackfruit --collection user_get_course_logs --type=csv --fields course_id,user_id --out data/user_get_course_logs.csv

>mongoexport --db jackfruit --collection courses --type=csv --fields _id,alias_name,name,code --out data/courses.csv

>mongoexport --db jackfruit --collection users --type=csv --fields _id,email --out data/users.csv

- Import data to MongoDB:

>./convert_data.sh


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Minhhc