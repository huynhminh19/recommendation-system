import time, sys, os
reload(sys)
sys.setdefaultencoding('utf-8')

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext, SparkSession
from pyspark.sql.functions import *
from pyspark.mllib.recommendation import ALS, MatrixFactorizationModel, Rating
from pyspark.mllib.linalg.distributed import IndexedRowMatrix, RowMatrix
import numpy as np
from mongo import client
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def init_spark_context():
    # load spark context
    conf = SparkConf().setAppName("recommendation-server")
    # IMPORTANT: pass aditional Python modules to each worker
    sc = SparkContext(conf=conf)

    return sc
class RecommendationProject:
    def __init__(self):
        self.ratings_RDD = None
        self.model = None

class RecommendationEngine:
    """A course recommendation engine
    """

    def __train_model(self, project):
        """Train the ALS model with the current dataset
        """
        logger.info("Training the ALS model...")
        self.projects[project].model = ALS.train(self.projects[project].ratings_RDD, self.rank, seed=self.seed,
                               iterations=self.iterations, lambda_=self.regularization_parameter)
        logger.info("ALS model built!")


    def __predict_sims(self, project):
        product_features = self.projects[project].model.productFeatures()
        mat = IndexedRowMatrix(product_features).toRowMatrix()
        mat = np.array(mat.rows.collect())
        mat = mat.transpose()
        transposed = self.sc.parallelize(mat)
        mat = RowMatrix(transposed)
        sims = mat.columnSimilarities()
        sims = sims.entries
        product_features = product_features.collect()
        sims = sims.map(lambda l: [product_features[l.i], product_features[l.j], l])
        sims = self.sqlContext.createDataFrame(sims)
        sims.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database", project).option("collection", "sims").save()

    def __predict_ratings(self, project):
        predicted_ratings = self.projects[project].model.recommendProductsForUsers(20).collect()
        predicted_ratings = map(lambda x: {"user": x[0], "top": list(x[1])}, predicted_ratings)
        predicted_ratings_df = self.sqlContext.createDataFrame(predicted_ratings)
        predicted_ratings_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database", project).option("collection", "predicted_user_ratings").save()

    def __predict_rating(self, project, user_id):
        predicted_ratings = self.projects[project].model.recommendProducts(user_id, 20)
        predicted_ratings = map(lambda x: {"user": x[0], "product": x[1], "rating": x[2]}, predicted_ratings)
        client[project].predicted_user_ratings.update_one({"user": user_id}, {"$set": {"top": predicted_ratings}}, upsert=True)

    def __predict_product_ratings(self, project):
        predicted_ratings = self.projects[project].model.recommendUsersForProducts(20).collect()
        predicted_ratings = map(lambda x: {"product": x[0], "top": list(x[1])}, predicted_ratings)
        predicted_ratings_df = self.sqlContext.createDataFrame(predicted_ratings)
        predicted_ratings_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database", project).option("collection", "predicted_product_ratings").save()

    def __predict_product_rating(self, project, item_id):
        predicted_ratings = self.projects[project].model.recommendUsers(item_id, 20)
        predicted_ratings = map(lambda x: {"user": x[0], "product": x[1], "rating": x[2]}, predicted_ratings)
        client[project].predicted_product_ratings.update_one({"product": item_id}, {"$set": {"top": predicted_ratings}}, upsert=True)

    def add_rating(self, project, ratings):
        """Add additional course ratings in the format (user_id, item_id, rating)
        """
        insert_ratings = map(lambda x: {"user_id": int(x[0]), "item_id": int(x[1]), "rating": float(x[2])}, ratings)
        client[project].ratings.insert_many(insert_ratings)

        new_ratings_RDD = self.sc.parallelize(ratings)
        self.projects[project].ratings_RDD = self.projects[project].ratings_RDD.union(new_ratings_RDD)
        # Re-train the ALS model with the new ratings
        self.__train_model(project)
        self.__predict_rating(project, int(ratings[0][0]))
        self.__predict_product_rating(project, int(ratings[0][1]))

    def recommendation(self, project):
        ratings_file_path = '../RestAPI/uploads/' + project
        ratings_raw_RDD = self.sc.textFile(ratings_file_path)
        ratings_raw_data_header = ratings_raw_RDD.take(1)[0]
        client["recommendation_admin"].projects.insert({"name": project})
        self.projects[project] = RecommendationProject()
        self.projects[project].ratings_RDD = ratings_raw_RDD.filter(lambda line: line!=ratings_raw_data_header)\
            .map(lambda line: line.split(",")).map(lambda tokens: (int(tokens[0]),int(tokens[1]),int(tokens[2])))

        # Train the model
        self.__train_model(project)
        self.__predict_ratings(project)
        self.__predict_product_ratings(project)
        self.__predict_sims(project)
        ratings_df = self.projects[project].ratings_RDD.toDF(['user_id', 'item_id', 'rating'])
        ratings_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database", project).option("collection", "ratings").save()

    def __init__(self):
        """Init the recommendation engine given a Spark context and a dataset path
        """
        self.projects = {}
        self.sc = init_spark_context()
        self.sqlContext = SQLContext(self.sc)
        self.ss = SparkSession(self.sc)
        self.rank = 10
        self.seed = 5L
        self.iterations = 10
        self.regularization_parameter = 0.1

        for project in client["recommendation_admin"].projects.find():
            project = project["name"]
            self.projects[project] = RecommendationProject()
            df = self.ss.read.format("com.mongodb.spark.sql.DefaultSource").option("uri", "mongodb://127.0.0.1/" + project + ".ratings").load()
            data = df.rdd
            self.projects[project].ratings_RDD = data.map(lambda l: (int(l["user_id"]),int(l["item_id"]),float(l["rating"])))
            self.__train_model(project)
