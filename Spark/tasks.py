import pika
import json
from engine import RecommendationEngine

recommendation_engine = RecommendationEngine()

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='recommendation')
channel.queue_declare(queue='add_buy')

def recommendation(ch, method, properties, body):
    print("Received %r" % body)
    recommendation_engine.recommendation(body)

def add_buy(ch, method, properties, body):
    print("Received %r" % body)
    data = json.loads(body)
    project = data["project"]
    rating = data["rating"].split(',')
    rating = (rating[0], rating[1], rating[2])
    recommendation_engine.add_rating(project, [rating])


channel.basic_consume(recommendation,
                      queue='recommendation',
                      no_ack=True)

channel.basic_consume(add_buy,
                      queue='add_buy',
                      no_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
