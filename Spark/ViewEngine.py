import time, sys, os
reload(sys)
sys.setdefaultencoding('utf-8')

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext, SparkSession
from pyspark.sql.functions import *
from pyspark.mllib.recommendation import ALS, MatrixFactorizationModel, Rating
from pyspark.mllib.linalg.distributed import IndexedRowMatrix

def init_spark_context():
    # load spark context
    conf = SparkConf().setAppName("recommendation-server")
    # IMPORTANT: pass aditional Python modules to each worker
    sc = SparkContext(conf=conf)

    return sc

sc = init_spark_context()
sqlContext = SQLContext(sc)

my_spark = SparkSession \
    .builder \
    .appName("myApp") \
    .getOrCreate()

df = my_spark.read.format("com.mongodb.spark.sql.DefaultSource").option("uri",
"mongodb://127.0.0.1/recommendation.views").load()
# Load and parse the data
df = df.groupBy("uid", "cid").agg(count("_id"))
data = df.rdd
ratings = data.map(lambda l: Rating(int(l[2]), int(l[1]), 1))

# Build the recommendation model using Alternating Least Squares
rank = 20
numIterations = 10
model = ALS.trainImplicit(ratings, rank, numIterations, alpha=0.01)

predicted_views = model.recommendProductsForUsers(20).collect()
predicted_views_df = sqlContext.createDataFrame(predicted_views)
predicted_views_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database", "recommendation").option("collection", "predicted_views").save()
