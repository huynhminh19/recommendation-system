import time, sys, os
reload(sys)
sys.setdefaultencoding('utf-8')

from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import *

def init_spark_context():
    # load spark context
    conf = SparkConf().setAppName("convert-server")
    # IMPORTANT: pass aditional Python modules to each worker
    sc = SparkContext(conf=conf)

    return sc

sc = init_spark_context()
sqlContext = SQLContext(sc)
dataset_path = os.path.join('data')

users_file_path = os.path.join(dataset_path, 'users.csv')
users_raw_RDD = sc.textFile(users_file_path)
header = users_raw_RDD.first()
users_rdd = users_raw_RDD.filter(lambda line: line != header).map(lambda line: line.split(",")).map(lambda tokens: (tokens[0],tokens[1])).zipWithIndex().map(lambda tokens: (tokens[0][0][tokens[0][0].index("(")+1:tokens[0][0].index(")")], tokens[0][1], tokens[1]))
users_df = users_rdd.toDF(['_id', 'email', 'uid'])
users_df.show()
users_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").save()

courses_file_path = os.path.join(dataset_path, 'courses.csv')
courses_raw_RDD = sc.textFile(courses_file_path)
header = courses_raw_RDD.first()
courses_rdd = courses_raw_RDD.filter(lambda line: line != header).map(lambda line: line.split(",")).map(lambda tokens: (tokens[0],tokens[1])).zipWithIndex().map(lambda tokens: (tokens[0][0][tokens[0][0].index("(")+1:tokens[0][0].index(")")], tokens[0][1], tokens[1]))
courses_df = courses_rdd.toDF(['_id', 'name', 'cid'])
courses_df.show()
courses_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database", "recommendation").option("collection", "courses").save()

buys_file_path = os.path.join(dataset_path, 'user_get_course_logs.csv')
buys_raw_RDD = sc.textFile(buys_file_path)
header = buys_raw_RDD.first()
buys_rdd = buys_raw_RDD.filter(lambda line: line != header).map(lambda line: line.split(",")).map(lambda tokens: (tokens[0][tokens[0].index("(")+1:tokens[0].index(")")],tokens[1][tokens[1].index("(")+1:tokens[1].index(")")]))
buys_df = buys_rdd.toDF(['course_id', 'user_id'])
buys_df = buys_df.join(users_df, users_df._id == buys_df.user_id).join(courses_df, courses_df._id == buys_df.course_id).select(users_df.uid, courses_df.cid)
buys_df.show()
buys_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database", "recommendation").option("collection", "buys").save()

# views_file_path = os.path.join(dataset_path, 'global_tracks.csv')
# views_raw_RDD = sc.textFile(views_file_path)
# header = views_raw_RDD.first()
# views_rdd = views_raw_RDD.filter(lambda line: line != header).map(lambda line: line.split(",")).map(lambda tokens: (tokens[0],tokens[1]))
# views_df = views_rdd.toDF(['user_id', 'course_url'])
# views_df = views_df.cube("user_id", "course_url").count().alias("cnt")
# views_df = views_df.join(users_df, users_df._id.contains(trim(views_df.user_id))).join(courses_df, views_df.course_url.contains(trim(courses_df.name))).select(users_df.uid, courses_df.cid, col("count"))
# views_df.write.format("com.mongodb.spark.sql.DefaultSource").mode("append").option("database", "recommendation").option("collection", "user_views").save()
# views_df.show()
